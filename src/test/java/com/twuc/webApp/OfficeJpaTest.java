package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class OfficeJpaTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void test() {
        assertTrue(true);
    }

    @Test
    public void should_save_an_entity() {
        Office savedOffice = officeRepository.save(new Office(1L, "Chengdu"));
        entityManager.flush();
        assertNotNull(savedOffice);
    }

    @Test
    public void should_save_an_entity_by_em() {
        entityManager.persist(new Office(1L, "Chengdu"));
        entityManager.flush();
        entityManager.clear();
        Office office = entityManager.find(Office.class, 1L);

        assertEquals(Long.valueOf(1), office.getId());
        assertEquals("Chengdu", office.getCity());
    }

    @Test
    public void should_throw_exception_when_city_is_null() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            officeRepository.save(new Office(1L, null));
            officeRepository.flush();
        });
    }

    @Test
    public void should_throw_exception_when_city_length_more_than_36() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            officeRepository.save(new Office(1L, "1234567891234567891234567891234567891"));
            officeRepository.flush();
        });
    }
}
