package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class StaffJpaTest {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void name() {
        assertTrue(true);
    }

    @Test
    public void should_save_an_entity() {
        Staff savedStaff = staffRepository.save(new Staff(1L, new Name("jiang", "zijie")));
        staffRepository.flush();
        assertNotNull(savedStaff);
    }

    @Test
    public void should_save_an_entity_with_em() {
        entityManager.persist(new Staff(1L, new Name("jiang", "zijie")));
        entityManager.flush();
        entityManager.clear();
        Staff staff = entityManager.find(Staff.class, 1L);
        assertEquals("jiang", staff.getName().getFirstName());
        assertEquals("zijie", staff.getName().getLastName());
    }
}
