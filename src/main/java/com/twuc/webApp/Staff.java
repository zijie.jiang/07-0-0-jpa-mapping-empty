package com.twuc.webApp;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.OverridesAttribute;

@Entity
public class Staff {
    @Id
    private Long id;

    @Embedded
    @AttributeOverrides(
            @AttributeOverride(name = "firstName", column = @Column(name = "contact_first_name"))
    )
    private Name name;

    public Staff() {
    }

    public Staff(Long id, Name name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Name getName() {
        return name;
    }
}
